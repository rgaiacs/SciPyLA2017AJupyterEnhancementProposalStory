# Talk Details

## Title

A Jupyter Enhancement Proposal Story

## Elevator Pitch

**You have 300 characters to sell your talk.**

If you use Jupyter
and don't like something on it,
you should know that you can contribute your idea to enhance the project.
At this talk you will learn how to submit a Jupyter Enhancement Proposals
and share your idea.

## Talk Format

Talk (~30-45 minutes)

## Audience Level

All

## Description

Python users should be familiar with the concept of
[Python Enhancement Proposals (PEPs)](https://www.python.org/dev/peps/),
the way that the Python language evolves over time.
In a similar fashion,
the Jupyter project has [Jupyter Enhancement Proposals (JEPs)](https://github.com/jupyter/enhancement-proposals).
This talk with cover the proposer first hand experience
when submiting [JEP 23 - Add Template as Metatada enhancement proposal](https://github.com/jupyter/enhancement-proposals/pull/23)
from it's begin,
during [EuroPython 2017](https://ep2017.europython.eu/),
until the current status of it.

## Notes

No technical requirements. No conflict of interest.

## Tags

- e-learning
- Jupyter
- Jupyter Notebook
- Jupyter Enhancement Proposals
- community

## Additional Information

- Do you require any specials meals?

Vegan.

- Are you a member of an under represented community?

No.

- Please provide a link to a video of a previous speaking engagement.

https://www.youtube.com/watch?v=B_C-GolpM6g

- Have you participated in a previous SciPyLA conference?

2016.

- Do you need any resources in-place to conduct activities successfully?

Don't apply

- Will you bring these resources (if any) with you? Do you expect them to be available at the venue otherwise?

No resources.

- If you bring the resources with you, would you consider donating them to the CubanTech Group so as to repeat the talk/workshop in some other provinces across the country?

Don't apply.

# Profile Details

## Name

Raniere Silva

## URL

http://rgaiacs.com/

## Organization or Affiliation

Software Sustainability Institute

## Twitter Handle

rgaiacs

## Shirt Size

Men's M

## Bio

I work as the Community Officer for the [Software Sustainability Institute](https://www.software.ac.uk/). When possible, I volunteer as a instructor/mentor at some local Python workshops, including [Software Carpentry](https://software-carpentry.org/) and [Django Girls Manchester](https://djangogirls.org/manchester). As part of my work and volunteer activities, I'm always looking for ways to improve the relationship between novice developers and their more experience fellows. In my spare time, I like to read sci-fi.

# Profile Details

## Name

Ivan Ogasawara

## URL

http://github.com/xmnlab

## Organization or Affiliation


## Twitter Handle

xmnlab

## Shirt Size

Men's M

## Bio

Ivan Ogasawara is a data scientist with expertise in private and public sectors in the areas such as: education, governance, business, health and transport engineering. 

He has experience with task such as:

* data analysis and visualization; 
* development of data acquisition from sensors,  statistical analysis, etc; 
* application of machine learning techniques and digital signal processing.

Currently, he is working developing SkData library.